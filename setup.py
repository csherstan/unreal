#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# To use a consistent encoding
import codecs
from setuptools import setup, find_packages
import sys, os.path

setup(
    name='unreal',
    version='0.0.1',
    description='',
    url='',
    author='Craig Sherstan',
    author_email='',
    license='',
    zip_safe=False,
    requires=["tensorflow"]
)
