"""
Feedforward inputs to the system are the current observation (S_t), the past reward (R_{t}) and past action (A_{t-1}).
Value loss is against the computed truncated return. G_t = R_{t+1} + gamma*R_{t+2} + ... + V(end of rollout), 
G_{t+1} = R_{t+2} + gamma*R_{t+3} + ... + V(end of rollout).


I made the decision that loss should be defined outside the model. The model is only a data structure.
"""

import unreal.core as core
import tensorflow as tf
from collections import namedtuple

from unreal.ioc import params


class Model(object):
    ProcessReturn = namedtuple("ProcessReturn", ["lstm_state", "pi", "value"])

    def __init__(self, action_count):
        self.last_action_t = tf.placeholder(tf.int32, [None], name="last_action")
        self.last_reward_t = tf.placeholder(tf.float32, [None], name="last_reward")

        # TODO: the shape of the observations shouldn't be defined in the Model. That should be task specific.
        # I imagine that providing the shape rather than just using None for things may provide ways of validating
        # the graph and perhaps allows for computational optimization to be performed.
        self.current_observation_t = tf.placeholder(tf.float32, [None, 84, 84, 3], name="current_observation")

        self.core = None
        self.variables = None
        self.policy = None
        self.value_estimator = None
        self.pixel_control = None

        # self.current_lstm_state = None

        with tf.variable_scope("model") as scope:
            with tf.variable_scope("core"):
                self.core = core.Core(action_count, self.current_observation_t, self.last_action_t,
                                      self.last_reward_t)
            with tf.variable_scope("policy"):
                self.policy = core.PolicyEstimator(self.core.get_outputs(), action_count)
            with tf.variable_scope("value"):
                self.value_estimator = core.ValueEstimator(self.core.get_outputs())

            with tf.variable_scope("pixel_control"):
                self.pixel_control = core.PixelControl(self.core.get_outputs(), action_count)

            self.variables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope.name)
            # for var in self.variables:
            #    print(var.name)

    def create_histograms(self):
        tf.summary.histogram("cv_1")

    def feed_forward_core(self, sess, last_action, last_reward, current_observation, state):
        """
        feed_forward does not modify the state of the LSTM
        
        :param sess: 
        :param last_action: 
        :param last_reward: 
        :param current_observation: 
        :param state:
        :return: 
        """

        # prep_feed_dict injects the lstm_state
        feed_dict = self.core.prep_feed_dict(state)
        feed_dict.update({self.last_action_t: last_action, self.last_reward_t: last_reward,
                          self.current_observation_t: current_observation})

        lstm_state, pi, value = sess.run([self.core.lstm_state, self.policy.estimator, self.value_estimator.estimator],
                                         feed_dict=feed_dict)

        return Model.ProcessReturn(lstm_state=lstm_state, pi=pi[0], value=value[0])

    # TODO: I'm sure there's a more elegant way to do all this. I'm keeping this separate just so we don't needlessly
    # run the policy estimator when we just want the value.
    # def feed_forward_value(self, sess, last_action, last_reward, current_observation, init_lstm_state):
    #     feed_dict = self.core.prep_feed_dict(init_lstm_state)
    #
    #     feed_dict.update({self.last_action_t: last_action, self.last_reward_t: last_reward,
    #                       self.current_observation_t: current_observation})
    #
    #     return sess.run([self.self.value_estimator.estimator],
    #                     feed_dict=feed_dict)

    # def reset(self):
    #     # reset memory components
    #     self.current_lstm_state = self.core.get_fresh_lstm_state()

    def ff_pixel_control_max_q(self, sess, global_slow_model, last_action, last_reward, current_observation,
                               lstm_state=None):
        # I'm making a deviation here. I'm going to use the original DDQN based approach from the dueling network paper
        # so we take the argmax on our current model, but the value from the global_slow_model
        lstm_state = lstm_state if lstm_state is not None else self.core.get_fresh_lstm_state()
        feed_dict = self.core.prep_feed_dict(lstm_state)
        feed_dict.update({self.last_action_t: last_action, self.last_reward_t: last_reward,
                          self.current_observation_t: current_observation})

        action_max = sess.run(self.pixel_control.action_max, feed_dict=feed_dict)

        feed_dict = global_slow_model.core.prep_feed_dict(lstm_state)
        feed_dict.update({global_slow_model.last_action_t: last_action, global_slow_model.last_reward_t: last_reward,
                          global_slow_model.current_observation_t: current_observation,
                          global_slow_model.pixel_control.actions: action_max})

        return sess.run(global_slow_model.pixel_control.q_a, feed_dict=feed_dict)
