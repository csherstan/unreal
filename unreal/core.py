import tensorflow as tf
import tensorflow.contrib.layers as layers
import numpy as np
import math

from unreal.ioc import params


def fc_initializer(input_channels, dtype=tf.float32):
    def _initializer(shape, dtype=dtype, partition_info=None):
        d = 1.0 / math.sqrt(input_channels)
        return tf.random_uniform(shape, minval=-d, maxval=d)

    return _initializer


def conv_initializer(w, h, input_channels, dtype=tf.float32):
    def _initializer(shape, dtype=dtype, partition_info=None):
        d = 1.0 / math.sqrt(w * h * input_channels)
        return tf.random_uniform(shape, minval=-d, maxval=d)

    return _initializer


# these initializers may need to get a bit more complicated: take params and return the initializer function
def weights_initializer(shape, dtype=tf.float32, partition_info=None):
    # TODO: more complicated initialization was used in the unreal reference project.
    return tf.truncated_normal(shape, stddev=0.1)


def biases_initializer(shape, dtype=tf.float32, partition_info=None):
    return tf.truncated_normal(shape=shape, stddev=0.1)


class Core(object):
    def __init__(self, action_count, current_observation, last_action, last_reward):
        self.conv_1 = None
        self.conv_2 = None
        self.fc = None
        self.lstm = None

        # initial_lstm state needs to be set before running the network
        self.initial_lstm_state = None
        self.initial_lstm_state0 = None
        self.initial_lstm_state1 = None

        self.lstm_outputs = None
        self.lstm_state = None
        self.value_estimator = None
        self.policy = None
        self.action_count = action_count
        self.variables = None

        self.fc_size = 256

        self.create_core(current_observation=current_observation, last_action=last_action, last_reward=last_reward)

    def create_histograms(self):
        tf.summary.histogram("conv_1", self.conv_1)
        tf.summary.histogram("conv_2", self.conv_2)
        tf.summary.histogram("fc", self.fc)
        tf.summary.histogram("lstm", self.lstm)

    @staticmethod
    def create_cnn(x, filters, kernel_size, stride):
        return layers.conv2d(
            inputs=x,
            num_outputs=filters,
            kernel_size=kernel_size,
            stride=stride,
            padding="VALID",
            activation_fn=tf.nn.relu,
            weights_initializer=conv_initializer(kernel_size, kernel_size, x.shape[3].value),
            biases_initializer=conv_initializer(kernel_size, kernel_size, x.shape[3].value))

    @staticmethod
    def create_fc(inputs, units, input_channels):
        return layers.fully_connected(inputs=inputs, num_outputs=units, activation_fn=tf.nn.relu,
                                      weights_initializer=fc_initializer(input_channels),
                                      biases_initializer=fc_initializer(input_channels))

    def create_lstm(self, inputs, units, initial_state_input):
        # self.lstm = tf.contrib.rnn.CoupledInputForgetGateLSTMCell(units, state_is_tuple=True)
        self.lstm = tf.contrib.rnn.BasicLSTMCell(units, state_is_tuple=True)

        # expanding the first dimension : [batch_size, max_time, ...]
        # what is max_time? I found a tutorial which referred to it as sequence length.
        # I've done the step size slightly different than the ref project. It should give a value of
        # 20, the rollout size, but this is a difference. It's a tensor so it's a bit of effort to evaluate.
        step_size = tf.shape(inputs)[:1]
        lstm_input = tf.expand_dims(inputs, [0])

        lstm_outputs, lstm_state = tf.nn.dynamic_rnn(self.lstm,
                                                     lstm_input,
                                                     initial_state=initial_state_input,
                                                     sequence_length=step_size,
                                                     time_major=False)

        # this reshaping is consistent with the ref project and with an A3C ref project
        return tf.reshape(lstm_outputs, [-1, units]), lstm_state

    def create_core(self, current_observation, last_action, last_reward):
        # input is 84x84x3 channels
        # [-1, 84, 84, 3]
        with tf.variable_scope("conv_1"):
            self.conv_1 = self.create_cnn(current_observation, filters=16, kernel_size=8, stride=4)
        with tf.variable_scope("conv_2"):
            self.conv_2 = self.create_cnn(self.conv_1, filters=32, kernel_size=4, stride=2)
        with tf.variable_scope("fc"):
            self.fc = self.create_fc(layers.flatten(self.conv_2), self.fc_size,
                                     input_channels=self.conv_2.shape[3].value)

        # LSTM layer. I don't yet understand what's going on with all the initialization
        # inputs are conv_2 + last_action + current_reward
        with tf.variable_scope("lstm"):
            self.initial_lstm_state0 = tf.placeholder(tf.float32, [1, self.fc_size], name="initial_lstm_state0")
            self.initial_lstm_state1 = tf.placeholder(tf.float32, [1, self.fc_size], name="initial_lstm_state1")
            self.initial_lstm_state = tf.contrib.rnn.LSTMStateTuple(self.initial_lstm_state0,
                                                                    self.initial_lstm_state1)
            last_action_one_hot = self.make_action_one_hot(last_action)
            # Copied. Not sure yet why we need to keep it as a class variable. Last action and reward
            lstm_inputs = tf.concat([self.fc, last_action_one_hot, tf.expand_dims(last_reward, axis=1)], 1)
            self.lstm_outputs, self.lstm_state = self.create_lstm(lstm_inputs, self.fc_size, self.initial_lstm_state)

    def get_outputs(self):
        return self.lstm_outputs

    def prep_feed_dict(self, init_lstm_state=None):
        if init_lstm_state is None:
            init_lstm_state = self.get_fresh_lstm_state()
        return {self.initial_lstm_state0: init_lstm_state[0], self.initial_lstm_state1: init_lstm_state[1]}

    def get_fresh_lstm_state(self):
        return tf.contrib.rnn.LSTMStateTuple(np.zeros([1, self.fc_size]), np.zeros([1, self.fc_size]))

    def make_action_one_hot(self, inputs):
        return tf.one_hot(inputs, self.action_count, dtype=tf.float32)


class PixelControl(object):
    def __init__(self, inputs, num_actions):
        self.num_actions = num_actions

        # first need a fc layer (output size is 32x7x7) with a relu output.
        # unreal_ref changes this to 32*9*9
        # the dimensions work out correctly this way.
        fc_layer = layers.relu(inputs=inputs, num_outputs=32 * 9 * 9,
                               weights_initializer=fc_initializer(9),
                               biases_initializer=fc_initializer(9))
        fc_layer_reshape = tf.reshape(fc_layer, [-1, 9, 9, 32])

        # A value and an advantage deconvoltional network is added. A deconv net is actually just upsampling
        # via convolution. in tensorflow these are conv2d_transpose
        # The target is the absolute change in pixel intensities. So we add a relu activation layer on the output
        # of these 2.
        # should be [None, 1]
        val_t = layers.conv2d_transpose(
            inputs=fc_layer_reshape,
            num_outputs=1,
            kernel_size=4,
            stride=2,
            padding="VALID",
            activation_fn=tf.nn.relu,
            weights_initializer=conv_initializer(4, 4, 32),
            biases_initializer=conv_initializer(4, 4, 32))

        # [None, 20, 20, num_actions]
        advantage_t = layers.conv2d_transpose(
            inputs=fc_layer_reshape,
            num_outputs=num_actions,
            kernel_size=4,
            stride=2,
            padding="VALID",
            activation_fn=tf.nn.relu,
            weights_initializer=conv_initializer(4, 4, 32),
            biases_initializer=conv_initializer(4, 4, 32))

        # [None, 20, 20, num_actions]
        avg_advantage_t = tf.reduce_mean(advantage_t, reduction_indices=3, keep_dims=True)

        # [-1, 20, 20, num_actions]
        self.estimator = val_t + advantage_t - avg_advantage_t

        # [-1, 20, 20]
        # For each region I want the action that produces the greatest change
        self.action_max = tf.argmax(self.estimator, axis=3)

        self.actions = tf.placeholder(tf.int32, [None, 20, 20])
        self.q_a = tf.reduce_sum(tf.multiply(tf.one_hot(self.actions, self.num_actions), self.estimator), axis=3,
                                 keep_dims=False)

    def loss(self, pixel_changes, actions):
        # the idea here is that we take the action selected at each time and see
        # what we estimated the resulting change would be, this returns a 20x20 image that
        # we estimated, then we compare against the actual change.
        # actions: [None]
        # pixel_changes: [None, 20, 20], this is not individual changes, but rather:
        #   pc_1 + gamma*pc_2 + gamma^2*pc_2 +....+ Q(s^T,argmax_a(Q(s^T,a, slow_network))

        # [None, num_actions]
        one_hot_actions = self.make_action_one_hot(actions)
        # [None, 1, 1, num_actions]
        one_hot_actions = tf.reshape(one_hot_actions, [-1, 1, 1, self.num_actions])

        # the multiplication selects the appropriate 20x20 prediction for each selected action
        # all other actions are zeroed out
        # [None, 20, 20, num_actions]
        mul = tf.multiply(self.estimator, one_hot_actions)

        # Now squash over the actions. This is just flattens to the predicted image on each timestep
        # [None, 20, 20]
        estimate = tf.reduce_sum(mul, axis=3, keep_dims=False)

        the_loss = tf.nn.l2_loss(pixel_changes - estimate)

        return the_loss

    def make_action_one_hot(self, inputs):
        return tf.one_hot(inputs, self.num_actions, dtype=tf.float32)


class ValueEstimator(object):
    def __init__(self, inputs):
        self.linear = layers.linear(inputs=inputs, num_outputs=1,
                                    weights_initializer=fc_initializer(inputs.shape[1].value),
                                    biases_initializer=fc_initializer(inputs.shape[1].value))
        self.estimator = tf.reshape(self.linear, [-1])


class PolicyEstimator(object):
    def __init__(self, inputs, action_count):
        # softmax will give a tensor the same size as the inputs.
        self.estimator = layers.linear(inputs=inputs, num_outputs=action_count, activation_fn=tf.nn.softmax,
                                       weights_initializer=fc_initializer(inputs.shape[1].value),
                                       biases_initializer=fc_initializer(inputs.shape[1].value))
