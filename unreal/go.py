#!/usr/bin/env python

import argparse
import multiprocessing
import threading

import tensorflow as tf
import zero_support as zs

from unreal.model import Model
from unreal.trainer import Trainer
import importlib
from unreal.summarizer import Summarizer
from unreal.utils import *
import time

import glob
import os
import signal

from unreal.ioc import params

"""
    Most of this file comes from https://github.com/awjuliani/DeepRL-Agents/blob/master/A3C-Doom.ipynb
"""

parser = argparse.ArgumentParser(description="description")
parser.add_argument('identifier', type=str)
parser.add_argument('cfg', type=str)
parser.add_argument('--max_threads', type=int, help='', default=None)
# parser.add_argument('--level', type=str, default="")
parser.add_argument('--timesteps', '-t', type=int, default=25000000)
parser.add_argument('--purge', action="store_true", default=False)

params.provide("replay_buffer_len", 2000)
params.provide("rollout_len", 20)
params.provide("core_gamma", 0.99)
params.provide("pc_replay_size", params["rollout_len"])
params.provide("clip", 40.0)
params.provide("sync_interval", 40000)


class Supervisor(object):
    def __init__(self, conf, coord, sess, log_loc, global_fast, global_slow):
        """
        It might be better if this same functionality was implemented using a message queue.
        :param sess: 
        :param log_loc: 
        :param global_fast: 
        :param global_slow: 
        """
        self.conf = conf
        self.coord = coord
        self.sess = sess
        self.writer = tf.summary.FileWriter(log_loc)

        self.rollout_summarizer = Summarizer(self.writer, "global/rollout")
        self.episode_summarizer = Summarizer(self.writer, "global/episode")
        self.ep_ts_summarizer = Summarizer(self.writer, "global/ep_ts")

        self.timesteps = 0
        self.global_timesteps_ph = tf.placeholder(tf.int32, name="global_timesteps_ph")
        self.global_timesteps = tf.Variable(self.timesteps, name="global_timesteps", trainable=False, dtype=tf.int32)
        self.global_timesteps_assign = self.global_timesteps.assign(self.global_timesteps_ph)
        self.learning_rate = self._make_learning_rate(conf.timesteps)
        self.episodes = 0

        self.global_fast = global_fast
        self.global_slow = global_slow
        self.last_sync = 0
        self.last_status = 0

        self.sync_interval = params["sync_interval"]

        self.start_time = None

        self.sync_op = sync_from(global_fast.variables, global_slow.variables)

        self.rollout_lock = threading.Lock()
        self.episode_lock = threading.Lock()

    def _make_learning_rate(self, max_t):
        # return tf.maximum(params["initial_learning_rate"] * tf.to_float(max_t - self.global_timesteps) / max_t, 0.0)
        return tf.constant(params["initial_learning_rate"])

    def start_the_clock(self):
        if self.start_time is None:
            self.start_time = time.time()

    def report_by_rollout(self, summaries, rollout_length):
        self.rollout_lock.acquire()

        summary = tf.Summary()
        summary.value.add(tag="alpha", simple_value=self.sess.run(self.learning_rate))
        summaries.append(summary)

        self.rollout_summarizer.increment(rollout_length)
        for summary in summaries:
            self.rollout_summarizer.add_summary(summary, 0)
        self.rollout_summarizer.flush()

        self.timesteps += rollout_length
        self.sess.run(self.global_timesteps_assign, feed_dict={self.global_timesteps_ph: self.timesteps})

        if self.timesteps - self.last_status >= 1000:
            diff = (time.time() - self.start_time)
            ts_rate = float(self.timesteps) / diff
            mil_rate = ts_rate * 60.0 * 60.0 / 1000000

            print("Stats: %d ts in %d s, %f ts/s, %f M ts/H" % (self.timesteps, diff, ts_rate, mil_rate))

            self.last_status = self.timesteps

        if self.timesteps - self.last_sync >= self.sync_interval:
            print("synchronizing slow model")

            self.sess.run(self.sync_op)

            self.last_sync = self.timesteps
        self.rollout_lock.release()

        if not self.coord.should_stop() and self.conf.timesteps is not None and self.timesteps > self.conf.timesteps:
            print("timestep limit ({}) reached, stopping.".format(self.conf.timesteps))
            self.coord.request_stop()

    def report_by_episode(self, summaries, episode_length):
        self.episode_lock.acquire()

        self.episode_summarizer.increment(1)
        self.ep_ts_summarizer.increment(episode_length)
        for summary in summaries:
            self.episode_summarizer.add_summary(summary, 0)
            self.ep_ts_summarizer.add_summary(summary, 0)

        self.episodes += 1

        # print("total episodes: %d" % self.episodes)

        self.episode_lock.release()


def main(conf):
    # TODO: should load the elements of cfg into the ioc server
    cfg = importlib.import_module(conf.cfg)
    config = tf.ConfigProto(log_device_placement=False, allow_soft_placement=True, device_count={"GPU": 0})

    file_dir = zs.get_file_dir(__file__)
    where = "/home/craig/tmp/unreal/log/{}".format(conf.identifier)

    if conf.purge:
        if os.path.isdir(where):
            for f in glob.glob(where + "/*"):
                os.remove(f)

    checkpoint_dir = "/home/craig/tmp/unreal/checkpoint"

    # with tf.device("/cpu:0"):
    with tf.variable_scope("global_fast"):
        global_fast_model = Model(cfg.action_size)
    with tf.variable_scope("global_steady"):
        global_slow_model = Model(cfg.action_size)

    with tf.Session(config=config) as sess:

        coord = tf.train.Coordinator()
        supervisor = Supervisor(conf, coord, sess, where, global_fast_model, global_slow_model)

        workers = []
        cpu_count = multiprocessing.cpu_count()
        thread_count = min(cpu_count, conf.max_threads) if conf.max_threads is not None else cpu_count
        for thread_num in range(thread_count):
            workers.append(
                Trainer(coord=coord, cfg=cfg, name=thread_num, supervisor=supervisor,
                        global_fast_model=global_fast_model, global_slow_model=global_slow_model,
                        rollout_size=params["rollout_len"]))

        supervisor.writer.add_graph(sess.graph)

        # graph_file = tf.summary.FileWriter(f"{file_dir}/graph", sess.graph)
        # graph_file.flush()
        # graph_file.close()

        saver = tf.train.Saver()
        checkpoint = tf.train.get_checkpoint_state(checkpoint_dir)
        if checkpoint and checkpoint.model_checkpoint_path:
            saver.restore(sess, checkpoint.model_checkpoint_path)
            print("checkpoint loaded: {}".format(checkpoint.model_checkpoint_path))

        sess.run(tf.global_variables_initializer())

        # if thread_count == 1:
        #     supervisor.start_the_clock()
        #     workers[0].work(coord, sess, conf.episodes, supervisor)
        #
        # else:
        supervisor.start_the_clock()
        for worker in workers:
            worker.start()

        def signal_handler(signal, frame):
            coord.request_stop()
            print('You pressed Ctrl+C! {}'.format(threading.current_thread()))

        signal.signal(signal.SIGINT, signal_handler)

        coord.join(workers)


if __name__ == "__main__":
    args = parser.parse_args()
    main(zs.DotDict(args.__dict__))
