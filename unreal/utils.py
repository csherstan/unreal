import tensorflow as tf
from collections import namedtuple


def sync_from(src_vars, dest_vars):
    ops = []
    for (src_var, dst_var) in zip(src_vars, dest_vars):
        ops.append(dst_var.assign(src_var))

    return tf.group(*ops)


def tuple_return(inputs):
    holder = namedtuple("holder", inputs.keys())
    return holder()
