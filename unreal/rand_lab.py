from unreal.environment.lab_environment import LabEnvironment
import argparse
import numpy as np

parser = argparse.ArgumentParser(description="description")
parser.add_argument('level', type=str)
parser.add_argument('episodes', type=int)

if __name__ == "__main__":
    args = parser.parse_args()
    env = LabEnvironment(args.level)

    avg_reward = 0.0
    for ep in range(args.episodes):
        env.reset()
        terminal = False
        ep_reward = 0.0
        while not terminal:
            action = np.random.choice(env.ACTIONS.keys())
            state, reward, terminal = env.process(action)
            ep_reward += reward

        avg_reward += (ep_reward - avg_reward)/(ep + 1)
        print("Ep: {}, reward: {}, avg reward: {}".format(ep, ep_reward, avg_reward))

    print("Avg reward {} over {} episodes".format(avg_reward, args.episodes))
