import tensorflow as tf
from unreal.model import Model
from unreal.experience import ExperienceTuple, ExperienceBuffer
from unreal.summarizer import Summarizer, SummarizerGroup
import numpy as np
from unreal.utils import *
from zero_support.dotdict import DotDict
import time
import threading
from tensorflow.python.client import timeline
import cProfile

from unreal.ioc import params

"""
    My hope was to split up the things that make up A3C from the model used. I had a worker and trainer class, but
    that caused me problems with needing to track model state. For now I'm going to combine Worker and Trainer, but
    I not that the only components here that are model specific are the loss and tracking the LSTM state. In the future
    perhaps I can find a way to separate them properly when I have more time.
"""


def create_pc_mask(mask_size, sq_img_size):
    pixel_change_mask = np.zeros((sq_img_size, sq_img_size / mask_size))
    for col in range(pixel_change_mask.shape[1]):
        row_start = col * mask_size
        pixel_change_mask[row_start:row_start + mask_size, col] = 1.0

    return pixel_change_mask


def compute_pixel_change(img_1, img_2, mask_one, mask_two, width):
    # exp.state: [84,84,3]
    # [84,84]
    channel_mean = np.mean(np.absolute(img_1 - img_2), 2)[2:-2, 2:-2]
    return np.dot(mask_two, np.dot(channel_mean, mask_one)) / width ** 2


class Trainer(threading.Thread):
    def __init__(self, coord, cfg, name, supervisor, global_fast_model, global_slow_model,
                 rollout_size=20):
        super(Trainer, self).__init__()
        self.coord = coord
        self.name = "trainer_{}".format(name)
        self.model = None
        self.supervisor = supervisor
        self.cfg = cfg
        self.optimizer = cfg.optimizer_factory(name=name, learning_rate=supervisor.learning_rate)
        self.action_count = cfg.action_size
        self.global_fast_model = global_fast_model
        self.global_slow_model = global_slow_model
        self.summaries = []

        self.apply_core_grads = None
        self.core_g_v = None
        self.sync_from_master = None
        self.rollout_size = rollout_size

        self.gamma = params["core_gamma"]
        self.grad_norm = None
        self.entropy = None
        self.value_loss = None
        self.policy_loss = None
        self.core_loss = None
        self.core_summaries = []
        self.core_summary_merge = None
        self.ret = None
        self.advantage = None
        self.actions = None
        self.values = None

        self.last_reward = 0
        self.last_action = 0

        # pixel changes
        self.pixel_changes = None
        self.pixel_change_actions = None
        self.pc_loss = None
        self.apply_pc_grads = None
        self.pc_g_v = None
        self.pc_summaries = []
        self.pc_summary_merge = None

        self.exp_buffer = ExperienceBuffer(params["replay_buffer_len"])

        with tf.variable_scope(self.name):
            self.model = Model(self.action_count)
            self.make_loss()
            self.make_pc_loss()

            self.apply_core_grads, self.core_g_v = self.make_grad_applier("core", self.core_loss,
                                                                          self.global_fast_model.variables,
                                                                          self.core_summaries)
            self.core_summary_merge = tf.summary.merge(self.core_summaries, name="core_merge")
            self.apply_pc_grads, self.pc_g_v = self.make_grad_applier("pc", self.pc_loss,
                                                                      self.global_fast_model.variables,
                                                                      self.pc_summaries)
            self.pc_summary_merge = tf.summary.merge(self.pc_summaries, name="pc_merge")

            self.sync_from_master = sync_from(self.global_fast_model.variables, self.model.variables)

        self.pc_width = 4
        self.pc_mask_one = create_pc_mask(self.pc_width, 80)
        self.pc_mask_two = np.transpose(self.pc_mask_one)

    def make_grad_applier(self, name, loss, global_fast_vars, summaries):
        grads = tf.gradients(loss, self.model.variables, gate_gradients=False, aggregation_method=None, colocate_gradients_with_ops=False)
        grads_and_vars = self.optimizer.compute_gradients(loss, self.model.variables)
        grads = []
        compatible_vars = []
        for gfv, gv in zip(global_fast_vars, grads_and_vars):
            if gv[0] is not None:
                # there was a major bug here, where I was appending gv[1] instead.
                grads.append(gv[0])
                compatible_vars.append(gfv)

        grads, grad_norms = tf.clip_by_global_norm(grads, params["clip"])

        g_v = zip(grads, compatible_vars)

        for g, v in g_v:
            summaries.append(tf.summary.histogram(v.name + "/" + name, v))
            summaries.append(tf.summary.histogram(v.name + "/" + name + "/grads", g))

        return self.optimizer.apply_gradients(g_v), g_v

        # g = tf.gradients(loss, self.model.variables)
        #
        # grads = []
        # compatible_vars = []
        # for gfv, gv in zip(global_fast_vars, g):
        #     if gv[0] is not None:
        #         grads.append(gv[1])
        #         compatible_vars.append(gfv)
        #
        # grads, grad_norms = tf.clip_by_global_norm(grads, params["clip"])
        # g_v = zip(grads, global_fast_vars)
        # return self.optimizer.apply_gradients(g_v), g_v

    def make_loss(self):
        self.ret = tf.placeholder(tf.float32, [None], name="return")
        # So I think this should be a tensor which is as long as the roll out. Advantage computed for each
        # observation I think.
        self.advantage = tf.placeholder(tf.float32, [None], name="advantage")
        self.actions = tf.placeholder(tf.int32, [None], name="actions")

        self.value_loss = tf.nn.l2_loss(self.ret - self.model.value_estimator.estimator)

        # loss

        """
        The idea here is that the loss is -E[log(pi(a|s))*Advantage]. So the bit in the log
        is only for the action taken, then we multiply by the advantage.
        """

        # I've confirmed this policy loss gives the same loss as
        #  https://github.com/tensorflow/tensorflow/blob/e69f71759adac4a794d5b159358af5253cb243bf/tensorflow/tensorboard/backend/application.py
        # clipped in case we have action w/ 0 probability
        log_pi = tf.log(tf.clip_by_value(self.model.policy.estimator, 1e-20, 1.0))

        action_one_hot = self.model.core.make_action_one_hot(self.actions)
        # I've confirmed the sign on this.
        self.policy_loss = -tf.reduce_sum(
            tf.reduce_sum(tf.multiply(log_pi, action_one_hot), reduction_indices=1) * self.advantage)

        # the more spread out the weighting the larger this number will be
        self.entropy = -tf.reduce_sum(self.model.policy.estimator * log_pi)

        self.core_loss = self.policy_loss - 0.001 * self.entropy + 0.5 * self.value_loss

        with tf.name_scope("summaries"):
            self.core_summaries.append(tf.summary.scalar("policy_loss", self.policy_loss))
            self.core_summaries.append(tf.summary.scalar("value_loss", self.value_loss))
            self.core_summaries.append(tf.summary.scalar("entropy", self.entropy))
            self.core_summaries.append(tf.summary.scalar("core_loss", self.core_loss))

    def make_pc_loss(self):
        self.pixel_changes = tf.placeholder(tf.float32, [None, 20, 20], name="pixel_changes")
        self.pixel_change_actions = tf.placeholder(tf.int32, [None], name="pixel_change_actions")

        # I use 0.05 for the pc_loss, that's what the ref project uses, but not what the paper uses, they
        # use a log-uniform distribution from 0.01-0.1
        self.pc_loss = 0.05 * self.model.pixel_control.loss(self.pixel_changes, self.pixel_change_actions)

        with tf.name_scope("summaries"):
            self.pc_summaries.append(tf.summary.scalar("pc_loss", self.pc_loss))

    def process_pixel_control(self):

        # I can't find any mention of how many frames they're processing for the pixel control on each update, but
        # may as well make it 20 to go with the ref project.
        # The first sample is going to be used only to provide the initial lstm_state, the last_reward and last_action
        # if a sample is at the terminal state it's next_state is None, the reward and action for that sample should not
        # be used.
        samples = self.exp_buffer.sample(params["pc_replay_size"] + 1)

        pc_loss = 0.0
        summary = tf.Summary()
        # if False:
        if len(samples) > 1:

            pixel_changes = []
            states = []
            last_actions = []
            last_rewards = []
            actions = []

            for idx, sample in enumerate(samples):

                if idx < (len(samples) - 1):
                    last_actions.append(sample.action)
                    last_rewards.append(sample.reward)

                if 0 < idx:
                    actions.append(sample.action)
                    states.append(sample.state)
                    pixel_changes.append(self.compute_pixel_change(sample))

            if len(states) > 0:

                # TODO: should have a more flexible way of setting this
                g = np.zeros([20, 20])

                if not samples[-1].terminal:
                    g = self.global_slow_model.ff_pixel_control_max_q(self.supervisor.sess, self.global_slow_model,
                                                                      [samples[-1].action],
                                                                      [samples[-1].reward],
                                                                      [samples[-1].next_state])[0]

                pixel_changes.reverse()
                returns = []
                for change in pixel_changes:
                    g = g * self.gamma + change
                    returns.append(g)

                returns.reverse()
                pixel_changes.reverse()

                # resetting the lstm state every time. TODO: I don't think this is necessary. Surely it should be better
                # if we have the LSTM state.
                feed_dict = self.model.core.prep_feed_dict()

                # self.last_action and self.last_reward get clobbered by process_core, plus they actually wouldn't be relevant
                # so we're going to the first sample from the replay before to provide last_reward and last_action.

                feed_dict.update({self.model.current_observation_t: states, self.model.last_action_t: last_actions,
                                  self.model.last_reward_t: last_rewards,
                                  self.actions: actions, self.pixel_change_actions: actions,
                                  self.pixel_changes: returns})

                _, pc_loss, summary = self.sess_run("process_pc",
                                                    [self.apply_pc_grads, self.pc_loss, self.pc_summary_merge],
                                                    feed_dict=feed_dict)

        # summary = tf.Summary()
        # summary.value.add(tag="pc_loss", simple_value=pc_loss)

        return DotDict(summary=summary, pc_loss=pc_loss)

    def train(self, environment, model_start_state):
        core_return = self.process_core(environment, model_start_state)
        pc_return = self.process_pixel_control()

        return core_return, pc_return

    def process_core(self, environment, model_start_state):
        # episode is a list, convert to np array so we can slice it.
        # each row is [s, v, a, r]

        # In the future, we could use partial_run, but documentation lists it as experimental still

        # is observation, t or t+1, what about reward and action?

        # When we use feedforward evaluation of the network what should be happening with the memory state of
        # the LSTM? So it should preserve the current memory. We are asking in context of the current stream of
        # experience so we want to evaluate the network without making any changes to its state.

        buff, terminal, reward_total, next_value, model_end_state = self.one_rollout(environment, model_start_state)

        summary = None

        if len(buff) > 0:

            self.exp_buffer.append(buff)

            g = next_value

            buff.reverse()

            returns = []
            advantages = []
            states = []
            rewards = []
            actions = []

            for exp in buff:
                g = g * self.gamma + exp.reward
                returns.append(g)
                advantages.append(g - exp.value)
                states.append(exp.state)
                rewards.append(exp.reward)
                actions.append(exp.action)

            returns.reverse()
            advantages.reverse()
            states.reverse()
            rewards.reverse()
            actions.reverse()

            # so now we're going to compute grads over the rollout. We need to initialize the lstm back to the start of
            # the rollout.
            feed_dict = self.model.core.prep_feed_dict(model_start_state)

            last_actions = [self.last_action] + actions[:-1]
            last_rewards = [self.last_reward] + rewards[:-1]

            feed_dict.update({self.model.current_observation_t: states, self.model.last_action_t: last_actions,
                              self.model.last_reward_t: last_rewards, self.advantage: advantages, self.ret: returns,
                              self.actions: actions})

            # before = self.global_fast_model.feed_forward_core(self.supervisor.sess, last_actions, last_rewards, states,
            #                                                   model_start_state)
            # print("before", before.value, before.pi)

            # feed_dict.update({self.model.current_observation_t: states, self.model.last_action_t: last_actions,
            #                   self.model.last_reward_t: last_rewards, self.advantage: advantages, self.ret: returns,
            #                   self.actions: actions, self.model.core.lstm_outputs: np.zeros((len(returns), 256))})

            self.last_reward = rewards[-1]
            self.last_action = actions[-1]

            # TODO: Perhaps we should validate that the lstm end state is the same as that returned by one_rollout
            _, loss, value_loss, policy_loss, g_v, entropy, val, summary = self.sess_run("process_core",
                                                                                         [self.apply_core_grads,
                                                                                          self.core_loss,
                                                                                          self.value_loss,
                                                                                          self.policy_loss,
                                                                                          self.core_g_v,
                                                                                          self.entropy,
                                                                                          self.model.value_estimator.estimator,
                                                                                          self.core_summary_merge],
                                                                                         feed_dict=feed_dict)
            # print(
            #     "val: {}, target: {}, loss: {}, value_loss: {}, policy_loss: {}, entropy: {}".format(val[0], returns[0],
            #                                                                                          loss,
            #                                                                                          value_loss,
            #                                                                                          policy_loss, entropy))

            # after = self.global_fast_model.feed_forward_core(self.supervisor.sess, last_actions, last_rewards, states,
            #                                                  model_start_state)
            # print("after", after.value, after.pi)

            # summary = tf.Summary()
            # summary.value.add(tag="loss", simple_value=loss)
            # summary.value.add(tag="v_loss", simple_value=value_loss)
            # summary.value.add(tag="policy_loss", simple_value=policy_loss)
            # summary.value.add(tag="entropy", simple_value=entropy)
            # summary.value.add(tag="rollout_reward", simple_value=reward_total)

        return DotDict(
            {"reward": reward_total, "rollout_len": len(buff), "model_end_state": model_end_state, "summary": summary})

    def one_rollout(self, environment, model_state):
        """

        :param environment: 
        :param model_state:
        :return: The return buffer will be len rollout_size (or shorter if terminated first).
        """
        # I think the idea is to fill the buffer to the rollout size, process, and then reset the buffer.
        buff = []

        reward_total = 0.0

        current_state = environment.get_last_state()

        # TODO: not sure if this matters, but perhaps I should handle the initial state better
        last_action_idx = environment.get_action_index(environment.get_last_action())
        last_reward = environment.get_last_reward()
        terminal = False

        for _ in range(self.rollout_size):
            model_return = self.model.feed_forward_core(self.supervisor.sess, [last_action_idx], [last_reward],
                                                        [current_state], state=model_state)
            model_state = model_return.lstm_state

            # if self.name == "trainer_0":
            #     print("pi:", model_return.pi, "value", model_return.value)
            action = self.choose_action(environment, model_return.pi)
            # action = "right"
            # print(action)
            state, reward, terminal = environment.process(action)
            action_idx = environment.get_action_index(action)
            reward_total += reward

            # the lab environment behaves a bit strangely. The agent doesn't know its in a terminal state until it tries
            # to take an action from that state.
            if terminal:
                state = None
                if len(buff) > 0:
                    exp = buff[-1]
                    buff.pop()
                    # TODO: this is ugly
                    buff.append(ExperienceTuple(state=exp.state, action=exp.action, reward=exp.reward, value=exp.value,
                                                next_state=exp.next_state, lstm_state=exp.lstm_state, terminal=True))
            else:
                # value is for the current_state
                buff.append(
                    ExperienceTuple(state=current_state, action=action_idx, reward=reward, value=model_return.value,
                                    next_state=state, lstm_state=model_return.lstm_state, terminal=False))

            last_action_idx = action_idx
            last_reward = reward
            current_state = state

            if terminal:
                break

        next_value = 0.0
        if not terminal:
            # does not affect the model_state being returned
            model_return = self.model.feed_forward_core(self.supervisor.sess, [last_action_idx], [last_reward],
                                                        [current_state],
                                                        state=model_state)
            next_value = model_return.value

        return buff, terminal, reward_total, next_value, model_state

    @staticmethod
    def choose_action(environment, pi):
        return np.random.choice(environment.get_actions(), p=pi)

    def run(self):
        profiling = False

        if profiling and self.name == "trainer_0":
            cProfile.runctx("self.twerk(coord)", globals(), locals(), sort="cumtime")
        else:
            self.twerk()

    def twerk(self):

        print("{} starting work".format(self.name))

        episode_count = 0
        episode_summarizer = Summarizer(self.supervisor.writer, "{}/perf/episode".format(self.name))
        rolling_summarizer = Summarizer(self.supervisor.writer, "{}/perf/rolling".format(self.name))

        environment = self.cfg.env_factory()
        self.last_reward = environment.get_last_reward()
        self.last_action = environment.get_action_index(environment.get_last_action())

        while not self.coord.should_stop():

            environment.reset()
            model_state = self.model.core.get_fresh_lstm_state()

            # rollout_summarizer = Summarizer(self.supervisor.writer, "{}/perf/rollout".format(self.name))

            episode_reward = 0.0
            episode_len = 0.0
            core_return = pc_return = None

            ep_t = time.time()

            while not self.coord.should_stop() and environment.is_running():
                self.sess_run("sync_from_master", self.sync_from_master)
                core_return, pc_return = self.train(environment, model_state)
                model_state = core_return.model_end_state
                episode_reward += core_return.reward
                # print("rollout_reward: {}, rollout_len: {}".format(rollout_reward, rollout_len))
                episode_len += core_return.rollout_len

                summary = tf.Summary()
                summary.value.add(tag="rollout_reward", simple_value=core_return.reward)

                summaries = [core_return.summary, pc_return.summary]
                rolling_summarizer.increment(core_return.rollout_len)
                for s in summaries:
                    rolling_summarizer.add_summary(s, 0)

                self.supervisor.report_by_rollout([summary], core_return.rollout_len)

                # sess.run(self.apply_grads, feed_dict={self.grads: grads})

                # TODO: need to add the ability to save and reload

            # if self.name == "trainer_1":
            #     print(
            #     "%s: %d ts/s, episode_len: %d, episode_reward: %f" % (threading.current_thread().name,
            #                                                         1000 * float(episode_len) / (time.time() - ep_t),
            #                                                              episode_len, episode_reward))

            print("episode_len: {}, episode_reward: {}".format(episode_len, episode_reward))

            summary = tf.Summary()
            summary.value.add(tag="episode_reward", simple_value=episode_reward)
            summary.value.add(tag="episode_length", simple_value=episode_len)
            episode_summarizer.add_summary(summary)
            episode_summarizer.flush()
            episode_count += 1

            episode_summarizer.add_summary(summary)

            self.supervisor.report_by_episode([summary], episode_len)

        environment.stop()
        print("{} terminating.".format(self.name))

    def compute_pixel_change(self, exp):
        # exp.state: [84,84,3]
        # [84,84]
        return compute_pixel_change(exp.state, exp.next_state, self.pc_mask_one, self.pc_mask_two, self.pc_width)

    def sess_run(self, name, *args, **kwargs):
        profile = False

        run_metadata = None
        if profile:
            kwargs["options"] = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
            kwargs["run_metadata"] = run_metadata

        ret = self.supervisor.sess.run(*args, **kwargs)

        if profile:
            fetched_timeline = timeline.Timeline(run_metadata.step_stats)
            chrome_trace = fetched_timeline.generate_chrome_trace_format()

            with open('{}_{}_{}.json'.format(self.name, name, self.supervisor.timesteps), 'w') as f:
                f.write(chrome_trace)

        return ret
