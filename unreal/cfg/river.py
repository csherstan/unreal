from unreal.environment.river import RiverEnvironment
from unreal.cfg.rms_prop import *

def env_factory(*args, **kwargs):
    return RiverEnvironment()

action_size = RiverEnvironment.get_action_size()
