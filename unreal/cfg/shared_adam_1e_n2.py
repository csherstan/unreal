import tensorflow as tf

_optimizer = tf.train.AdamOptimizer(1e-2)


def optimizer_factory(*args, **kwargs):
    return _optimizer

