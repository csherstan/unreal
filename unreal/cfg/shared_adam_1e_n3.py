import tensorflow as tf

_optimizer = tf.train.AdamOptimizer(1e-3)


def optimizer_factory(*args, **kwargs):
    return _optimizer

