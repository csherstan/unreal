from unreal.environment.lab_environment import LabEnvironment
from unreal.cfg.rms_prop import *


def env_factory(*args, **kwargs):
    return LabEnvironment(level="seekavoid_arena_01")

action_size = LabEnvironment.get_action_size()
