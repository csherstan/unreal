import tensorflow as tf
import math
from unreal.ioc import params

# _optimizer = tf.train.RMSPropOptimizer(log_uniform(1e-4, 5e-3, 5))
_optimizer = None


# taken from ref project, this doesn't actually draw from a distribution...
def log_uniform(lo, hi, rate):
    log_lo = math.log(lo)
    log_hi = math.log(hi)
    v = log_lo * (1 - rate) + log_hi * rate
    return math.exp(v)


params.provide("initial_learning_rate", 0.5*log_uniform(1e-4, 5e-3, 0.5))

print(params["initial_learning_rate"])


# TODO: all this should be converted to use the ioc module.
def optimizer_factory(*args, **kwargs):
    global _optimizer
    if _optimizer is None:
        # ref project does not use locking
        _optimizer = tf.train.RMSPropOptimizer(kwargs["learning_rate"], use_locking=True)
    return _optimizer
