from unreal.environment.fake_environment import FakeEnvironment
# from unreal.cfg.shared_adam_1e_n2 import *
from unreal.cfg.rms_prop import *

def env_factory(*args, **kwargs):
    return FakeEnvironment(episode_len=300)

action_size = FakeEnvironment.get_action_size()