import tensorflow as tf
import os
import glob
import threading


class Summarizer(object):
    def __init__(self, writer, name=None):

        self.writer = writer
        # self.merge_all = tf.summary.merge_all()
        self.counter = 0
        self.lock = threading.Lock()
        self.name = name

    def add_summary(self, summary, increment=1):
        # TODO: Need a separate summarizer for each type of thing I'm summarizing. Otherwise counts are mixed up
        # the writer should be thread safe, but the counter isn't
        if type(summary) is tf.Summary and self.name is not None:
            summary_copy = tf.Summary()
            prefix = self.name + "/"
            for v in summary.value:
                tag = v.tag
                if not tag.startswith(prefix):
                    tag = prefix + tag

                summary_copy.value.add(tag=tag, simple_value=v.simple_value)
                summary = summary_copy

        self.lock.acquire()
        # print("counter:", self.counter)
        self.writer.add_summary(summary, self.counter)
        self.counter += increment
        self.lock.release()

    def flush(self):
        # self.writer.flush()
        pass

    def increment(self, steps):
        self.lock.acquire()
        self.counter += steps
        self.lock.release()


class SummarizerGroup(object):
    def __init__(self, summarizers):
        self.summarizers = summarizers

    def add_summary(self, summary):
        for summarizer in self.summarizers:
            summarizer.add_summary(summary)

    def flush(self):
        for summarizer in self.summarizers:
            summarizer.flush()