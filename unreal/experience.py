from collections import namedtuple, deque
import numpy as np
import itertools

ExperienceTuple = namedtuple("ExperienceTuple",
                             ["state", "action", "reward", "value", "next_state", "lstm_state", "terminal"])


class ExperienceBuffer(object):
    """
    One of these per worker. 
    
    """

    def __init__(self, maxlen):
        self.q = deque(maxlen=maxlen)
        self.rewarding = deque(maxlen=maxlen)
        self.non_rewarding = deque(maxlen=maxlen)

        # I don't like this approach, should be a way to keep references
        self.head = 0
        self.tail = 0

    def append(self, frames):

        overlap = max(len(self.q) + len(frames) - self.q.maxlen, 0)

        if overlap > 0:
            self.tail += overlap

            self._clean_q(self.rewarding)
            self._clean_q(self.non_rewarding)

        self.q.extend(frames)

        for frame in frames:
            if frame.reward == 0:
                self.non_rewarding.append(self.head)
            else:
                self.rewarding.append(self.head)
            self.head += 1

    def sample(self, num_frames):

        the_q = self.non_rewarding
        if np.random.randint(0, 2) == 1:
            the_q = self.rewarding

        # TODO: this is a hack because I'm having problems with neg vals
        # self._clean_q(the_q)

        start = 0
        end = 0
        if len(the_q) > 0:
            start = np.random.choice(the_q) - self.tail
            if start < 0:
                print("neg start {}, tail: {}".format(start, self.tail))
                start = 0
            end = start + num_frames

        pre_filter = list(itertools.islice(self.q, start, end))

        # if we encounter the end of an episode we terminate the returned samples
        post_filter = []
        for sample in pre_filter:
            post_filter.append(sample)
            if sample.terminal:
                break

        return post_filter

    def _clean_q(self, the_q):
        while len(the_q) > 0 and the_q[0] < self.tail:
            the_q.popleft()
