from unreal.environment.environment import Environment
from collections import OrderedDict
import numpy as np


class FakeEnvironment(Environment):
    ACTIONS = OrderedDict(sorted({"left": -1.0, "none": 0, "right": 1.0}.items(), key=lambda t: t[0]))

    def __init__(self, episode_len=None):
        self.draw = 0
        self.last_observation = self.get_observation()
        self.terminal = False
        self.last_action = "none"
        self.lim = episode_len

    def reset(self):
        self.draw = 0
        self.terminal = False

    def process(self, action):
        self.draw += 1
        if self.lim is not None and self.draw >= self.lim:
            self.terminal = True

        reward = self.ACTIONS[action]

        return self.last_observation, reward, self.terminal

    def is_running(self):
        return not self.terminal

    def get_observation(self):
        # single state
        obs = np.ones((84, 84, 3))
        return obs

    def get_last_state(self):
        return self.last_observation

    def get_last_reward(self):
        return 1.0

    def get_last_action(self):
        return self.last_action

    def stop(self):
        pass
