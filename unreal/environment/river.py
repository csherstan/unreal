from unreal.environment.environment import Environment
from collections import OrderedDict
import numpy as np


class RiverEnvironment(Environment):
    ACTIONS = OrderedDict(sorted({"left": -1.0, "right": 1.0}.items(), key=lambda t: t[0]))

    def __init__(self, length=21):
        self.step = 0
        self.length = length
        self.pos = self.start_pos = int(self.length/2)
        self.last_observation = self.get_observation()
        self.terminal = False
        self.last_action = "left"
        self.last_reward = 0.0

    def reset(self):
        self.step = 0
        self.terminal = False
        self.pos = self.start_pos
        self.last_reward = 0.0
        self.last_action ="left"

    def process(self, action):
        self.step += 1

        # the lab domain doesn't let you know its terminal until you try and take an action out of the terminal state
        self.terminal = self.pos == 0 or self.pos == (self.length - 1)

        if not self.terminal:
            if action == "left":
                self.pos -= 1
                self.last_reward = -1.0
            elif action == "right":
                self.pos += 1
                self.last_reward = 1.0
            else:
                assert "Bad action"

            # self.last_reward = self.pos
            self.last_observation = self.get_observation()
        else:
            self.last_reward = 0.0

        return self.last_observation, self.last_reward, self.terminal

    def is_running(self):
        return not self.terminal

    def get_observation(self):
        obs = np.zeros((84, 84, 3))
        obs[self.pos, 0, 0] = 1.0
        return obs

    def get_last_state(self):
        return self.last_observation

    def get_last_reward(self):
        return self.last_reward

    def get_last_action(self):
        return self.last_action

    def stop(self):
        pass
