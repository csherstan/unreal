from unreal.environment.environment import Environment
import numpy as np
import deepmind_lab
from collections import OrderedDict

from multiprocessing import Process, Pipe

"""
This is very much based off of the code from the ref project.

There is an issue where the lab environment blocks other threads if this is run multithreaded. So instead
we use multiprocessing.
"""

COMMAND_RESET = 0
COMMAND_ACTION = 1
COMMAND_TERMINATE = 2
COMMAND_STATUS = 3


def worker(conn, level):
    env = deepmind_lab.Lab(
        level,
        ['RGB_INTERLACED'],
        config={
            'fps': str(60),
            'width': str(84),
            'height': str(84)
        })
    conn.send(0)

    while True:
        packet = conn.recv()
        command = packet[0]

        if command == COMMAND_RESET:
            env.reset()
            obs = env.observations()['RGB_INTERLACED']
            conn.send(obs)
        elif command == COMMAND_ACTION:
            reward = env.step(packet[1], num_steps=4)
            terminal = not env.is_running()
            if not terminal:
                obs = env.observations()['RGB_INTERLACED']
            else:
                obs = 0
            conn.send([obs, reward, terminal])
        elif command == COMMAND_STATUS:
            conn.send(env.is_running())
        elif command == COMMAND_TERMINATE:
            break
        else:
            print("bad command: {}".format(command))
    env.close()
    conn.send(0)
    conn.close()


def _action(*entries):
    return np.array(entries, dtype=np.intc)


def _preprocess_frame(image):
    image = image.astype(np.float32)
    image = image / 255.0
    return image


class LabEnvironment(Environment):
    ACTIONS = OrderedDict(sorted({
        'look_left': _action(-20, 0, 0, 0, 0, 0, 0),
        'look_right': _action(20, 0, 0, 0, 0, 0, 0),
        # 'look_up': _action(0, 10, 0, 0, 0, 0, 0),
        # 'look_down': _action(0, -10, 0, 0, 0, 0, 0),
        'strafe_left': _action(0, 0, -1, 0, 0, 0, 0),
        'strafe_right': _action(0, 0, 1, 0, 0, 0, 0),
        'forward': _action(0, 0, 0, 1, 0, 0, 0),
        'backward': _action(0, 0, 0, -1, 0, 0, 0),
        # 'fire': _action(0, 0, 0, 0, 1, 0, 0),
        # 'jump': _action(0, 0, 0, 0, 0, 1, 0),
        # 'crouch': _action(0, 0, 0, 0, 0, 0, 1)
    }.items(), key=lambda t: t[0]))
    # ACTIONS = OrderedDict({
    #     'look_left': _action(-20, 0, 0, 0, 0, 0, 0),
    #     'look_right': _action(20, 0, 0, 0, 0, 0, 0),
    #     'look_up': _action(0, 10, 0, 0, 0, 0, 0),
    #     'look_down': _action(0, -10, 0, 0, 0, 0, 0),
    #     'strafe_left': _action(0, 0, -1, 0, 0, 0, 0),
    #     'strafe_right': _action(0, 0, 1, 0, 0, 0, 0),
    #     'forward': _action(0, 0, 0, 1, 0, 0, 0),
    #     'backward': _action(0, 0, 0, -1, 0, 0, 0),
    #     'fire': _action(0, 0, 0, 0, 1, 0, 0),
    #     'jump': _action(0, 0, 0, 0, 0, 1, 0),
    #     'crouch': _action(0, 0, 0, 0, 0, 0, 1),
    #     'none': _action(0, 0, 0, 0, 0, 0, 0)
    # })

    def __init__(self, level):
        Environment.__init__(self)
        self.level = level

        self.conn, worker_conn = Pipe()
        self.proc = Process(target=worker, args=(worker_conn, self.level))
        self.proc.start()
        self.conn.recv()

        # TODO: I'm not sure I like storing this with the environment
        self.last_state = None
        self.last_action = None
        self.last_reward = None

        self.reset()

    def reset(self):
        self.conn.send([COMMAND_RESET, 0])
        self.last_state = _preprocess_frame(self.conn.recv())
        self.last_action = "backward"
        self.last_reward = 0

    def process(self, action):
        real_action = self.ACTIONS[action]

        # TODO: num_steps should be configurable
        self.conn.send([COMMAND_ACTION, real_action])
        obs, reward, terminal = self.conn.recv()

        if not terminal:
            state = _preprocess_frame(obs)
        else:
            state = self.last_state

        self.last_state = state
        self.last_action = action
        self.last_reward = reward
        return state, reward, terminal

    def stop(self):
        self.conn.send([COMMAND_TERMINATE, 0])
        self.conn.recv()
        self.conn.close()
        self.proc.join()
        print("lab environment stopped")

    def is_running(self):
        self.conn.send([COMMAND_STATUS, 0])
        return self.conn.recv()

    def get_last_action(self):
        return self.last_action

    def get_last_reward(self):
        return self.last_reward

    def get_last_state(self):
        return self.last_state
