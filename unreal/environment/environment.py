"""
 Modified from the ref project.
"""
from collections import OrderedDict
# -*- coding: utf-8 -*-


class Environment(object):

    ACTIONS = OrderedDict({})

    def __init__(self):
        pass

    @classmethod
    def get_action_size(cls):
        return len(cls.ACTIONS)

    @classmethod
    def get_actions(cls):
        return list(cls.ACTIONS.keys())

    @classmethod
    def get_action_index(cls, action):
        return list(cls.ACTIONS.keys()).index(action)

    def reset(self):
        raise NotImplementedError

    def process(self, action):
        raise NotImplementedError

    def is_running(self):
        raise NotImplementedError

    def get_observation(self):
        raise NotImplementedError

    def get_last_state(self):
        raise NotImplementedError

    def get_last_reward(self):
        raise NotImplementedError

    def get_last_action(self):
        raise NotImplementedError

    def stop(self):
        raise NotImplementedError
